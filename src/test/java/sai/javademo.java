package sai;

import java.io.IOException;

import java.net.URL;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class javademo {

	public static void main(String[] args) throws IOException {
		
		
		String url="http://192.168.0.12:4444/wd/hub";
		DesiredCapabilities desiredCapabilities =new DesiredCapabilities();
		desiredCapabilities.setBrowserName("chrome");
		desiredCapabilities.setPlatform(Platform.WINDOWS);
		WebDriver driver = new RemoteWebDriver((new URL(url)), desiredCapabilities);
		driver.get("https://www.google.com");
		String pageTitle=driver.getTitle();
		System.out.println(pageTitle);
		driver.quit();
		System.out.println("Test sucess");
		
	}

}
